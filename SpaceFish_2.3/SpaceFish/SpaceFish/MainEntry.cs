#region Includes
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Runtime;
using System.Runtime.Serialization;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using System;
using SpaceFish.Screen;
#endregion


#region program start

namespace SpaceFish
{
    //Main Application constructor
    public class Main : Microsoft.Xna.Framework.Game
    {
        public ScreenManager ScreenManager;

        #region Base variables
        //setting the variables to public static so that they can be accessed by classes
            public static SpriteBatch spriteBatch;
        //creating a vector for screen size
            public static Vector2 screensize;   
        //declare Audio Handler for use
            private AudioHandler audioHandler;
        //declare Input Handler
            private InputHandler inputHandler { get; set; }
        // declare GraphicsHandler
            private GraphicsHandler graphicsHandler;
        //State Machine
            public enum GameState { STARTMENU,LOADING,OPTIONS, PLAYING, PAUSED }
            public static GameState gameState;
        //declaring the font for use
            public static string Layertext;
        //Threading
            private Thread backgroundThread;
        //Mouse
             MouseState CurrentState;
             MouseState previousMouseState;
        //texturedaa needing cleaned still

            // private Texture2D loadingScreen;      

             private bool isLoading;
             bool isMusicPlaying;
             bool isGameStarted;

             Matrix Transform;
        #endregion 

        //Main Application constructor
        public Main()
        {
            Content.RootDirectory = "Content";
            
            // Frame rate is 30 fps by default for Windows Phone.
            TargetElapsedTime = TimeSpan.FromTicks(333333);

            // Extend battery life under lock.
            InactiveSleepTime = TimeSpan.FromSeconds(1);

            graphicsHandler = new GraphicsHandler(this);

            inputHandler = new InputHandler();

            audioHandler = new AudioHandler();
        }
    
        //All initalisations happen here
        protected override void Initialize()
        {
            isMusicPlaying = false;
            isGameStarted = false;

            //Make the mouse visible
            this.IsMouseVisible = true;

            //Initialise the size
            screensize = new Vector2(Window.ClientBounds.Width, Window.ClientBounds.Height);

            //enable the mousepointer
            IsMouseVisible = true;

            //set the gamestate to start menu
            gameState = GameState.STARTMENU;
            
            //get the mouse state
            CurrentState = Mouse.GetState();
            previousMouseState = CurrentState;
         
            //initialise the input handler
            inputHandler.Initialise(1, 1);

            //initialise the audio handler
            audioHandler.Initialize();

            // Initialise the Graphics Handler
            graphicsHandler.Initialise(GraphicsDevice);

            Transform = Matrix.CreateTranslation(-32, 0, 0);
            ScreenManager = new ScreenManager(Content, inputHandler);
            
            base.Initialize();
        }

        //Loading of content
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(graphicsHandler.GetGraphicsDevice());

            //load the loading screen
            graphicsHandler.loadingScreen.LoadLoadingScreenContent(Content);
            //loadingScreen = Content.Load<Texture2D>("Content/Graphics/LoadingThread/Loading");

            //Creat a  new font
            graphicsHandler.gameFont.LoadFontContent(Content);
            ///Font = Content.Load<SpriteFont>("Content/Graphics/Fonts/Font");

            //load in audio songs
            audioHandler.LoadInSongs(0, Content.Load<Song>("Content/Audio/MainMenu/Menu"));
            audioHandler.LoadInSongs(1, Content.Load<Song>("Content/Audio/MainMenu/SpaceMenu"));
            audioHandler.LoadInSongs(2, Content.Load<Song>("Content/Audio/MainMenu/Above"));
        }
        //unloading content
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        //all updating on screen happens here
        protected override void Update(GameTime gameTime)
        {
            inputHandler.mouseState = CurrentState;
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                gameState = GameState.STARTMENU;
                //stop the music and reset variables
                audioHandler.StopPlaying();
                isMusicPlaying = false;
                isGameStarted = false;
            }

            //if the track is not currently playing or paused, play first song in list
            if (isMusicPlaying == false && isGameStarted == false)
            {
                audioHandler.PlaySong(0);
                isMusicPlaying = true;
            }

            if (isMusicPlaying == true && isGameStarted == false)
            {
                if (MediaPlayer.State != MediaState.Playing)
                {
                    //move to next song in the list
                    audioHandler.MoveToNextSong();

                    //if the next song doesn't exist, then set the first song to play again
                    if (audioHandler.GetCurrentSong() > audioHandler.GetSizeOfList() - 1)
                    {
                        //return the list to the start
                        audioHandler.SetCurrentSong(0);
                    }

                    //play the first song
                    audioHandler.PlaySong(audioHandler.GetCurrentSong());
                }
            }

            //Update mouse state
            CurrentState = Mouse.GetState();

            //load the game when needed
            if (gameState == GameState.LOADING && !isLoading) //isLoading bool is to prevent the LoadGame method from being called 60 times a seconds
            {
                //set background thread
                backgroundThread = new Thread(LoadGame);
                backgroundThread.Name = "Loading Thread";
                isLoading = true;
                
                isGameStarted = true;
                isMusicPlaying = false;

                //start background thread
                backgroundThread.Start();
                
            }

            if (gameState == GameState.STARTMENU)
            {
                ScreenManager.MainMenuObj.Update(gameTime);
            }

            //move the orb if the game is in progress (Playing LOGIC)
            if (gameState == GameState.PLAYING)
            {
                //if the game is started play the in game song
                if (isGameStarted == true && isMusicPlaying == false)
                {
                    audioHandler.PlaySong(2);
                    isMusicPlaying = true;
                }

                ScreenManager.GameObj.Update(gameTime);
            }

            base.Update(gameTime);
        }
        //content draw
        protected override void Draw(GameTime gameTime)
        {
            graphicsHandler.ClearGraphicsDevice(Color.CornflowerBlue);
            spriteBatch.Begin();

            if (gameState == GameState.STARTMENU)
            {
                ScreenManager.MainMenuObj.Draw();
                Layertext = "MainMenu";
                spriteBatch.DrawString(graphicsHandler.gameFont.GetGameFont(), Layertext, new Vector2(10, 10), Color.White);
            }

            //show the loading screen when needed
            if (gameState == GameState.LOADING)
            {
                spriteBatch.Draw(graphicsHandler.loadingScreen.GetLoadingScreen(), graphicsHandler.GetGraphicsDeviceViewport(graphicsHandler.loadingScreen.GetLoadingScreen()), Color.YellowGreen);
                ///spriteBatch.Draw(loadingScreen, new Vector2((graphicsHandler.graphicsDevice.Viewport.Width / 2) - (loadingScreen.Width / 2), (graphicsHandler.graphicsDevice.Viewport.Height / 2) - (loadingScreen.Height / 2)), Color.YellowGreen);
            }

            //game is in progress (Playing LOGIC)
            if (gameState == GameState.PLAYING)
            {
                ScreenManager.GameObj.Draw();
                Layertext = "Playing";
                spriteBatch.DrawString(graphicsHandler.gameFont.GetGameFont(), Layertext, new Vector2(10, 10), Color.White);
            }

            //draw the pause screen
            if (gameState == GameState.PAUSED)
            {

            }
            spriteBatch.End();

            base.Draw(gameTime);
        }
    
        #region loading
        void LoadGame()
        {

            //since this will go to fast for this demo's purpose, wait for ??? milliseconds
            Thread.Sleep(750);

            //start playing
            gameState = GameState.PLAYING;
        }
        #endregion
    }
}
#endregion