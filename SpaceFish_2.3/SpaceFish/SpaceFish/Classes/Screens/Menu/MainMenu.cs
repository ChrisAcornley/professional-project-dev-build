#region Includes
using System;

using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Runtime;
using System.Runtime.Serialization;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using SpaceFish.Button;
using SpaceFish.Sprite;
using SpaceFish.Screen;
#endregion

namespace SpaceFish.Screen
{
    public class MainMenuClass : ScreenClass
    {

        //the manager cannot be made in the screen as it causes an infinite loop
        Button.ButtonManager BM;
        
        ContentManager ContentM;

        //Declaring buttons textures
            Texture2D Backgroundtex;
        //Declar the position of hud
            Vector2   BackgroundPosition;
        //Declaring the playbuttons for use
            Texture2D Button1tex;
            Vector2   Button1Position;
        //Declaring the playbuttons for use
            Texture2D Button2tex;
            Vector2 Button2Position;



            public MainMenuClass(ContentManager Contentm, ScreenManager ScreenM)
        {
            //
            BM = new Button.ButtonManager(ScreenM);

            ContentM = Contentm;

            //Texture data for the menu
            Backgroundtex = ContentM.Load<Texture2D>("Content/Graphics/MainMenu/Background");
            //Initialise the position
            BackgroundPosition = new Vector2((int)Main.screensize.X - (int)Main.screensize.X, (int)Main.screensize.Y - (int)Main.screensize.Y);
            //Texture data for the menu
            Button1tex = ContentM.Load<Texture2D>("Content/Graphics/MainMenu/Start"); ;
            //inialise the position of the button
            Button1Position = new Vector2(BackgroundPosition.X + 50, BackgroundPosition.Y +400 );
            //Texture data for the menu
            Button2tex = ContentM.Load<Texture2D>("Content/Graphics/MainMenu/Option"); ;
            //inialise the position of the button
            Button2Position = new Vector2(BackgroundPosition.X + 300, BackgroundPosition.Y + 400);

            //create the buttons and add them to the list
            buttonsList.Add(new ButtonClass(Button1tex, new Vector2(Button1Position.X, Button1Position.Y),0 ));
            buttonsList.Add(new ButtonClass(Button2tex, new Vector2(Button2Position.X, Button2Position.Y),1 ));

            //subscribe to each button
            BM.Subscribe(buttonsList[0]);
            BM.Subscribe(buttonsList[1]);

            //generic sprite for the background
            sprites.Add(new SpriteClass(Backgroundtex, new Vector2(BackgroundPosition.X, BackgroundPosition.Y),false));
          
            //so the music stuff
            SongMenu = Contentm.Load<Song>("Content/Audio/MainMenu/SpaceMenu");
            SongGame = Contentm.Load<Song>("Content/Audio/MainMenu/SpaceMenu");
            SongList.Add(SongMenu);


        }
            public void Update(GameTime gt)
        {

            // loop through the list of sprites and update them
            for (int i = 0; i < sprites.Count(); i++)
            {
                sprites[i].Update(gt);
            }

            //same for the buttons
            for (int i = 0; i < buttonsList.Count(); i++)
            {
                buttonsList[i].Update(gt);
            }
            

            //if the track is not currently playing or paused, play song
            if (isMusicPlaying == false && Main.gameState == Main.GameState.STARTMENU)
            {
                MediaPlayer.Play(SongMenu);
                isMusicPlaying = true;
            }
            else if (Main.gameState == Main.GameState.PLAYING)
            {
                MediaPlayer.Stop();
            }


        }
        public void Draw()
        {
            //draw the sprites
            for (int i = 0; i < sprites.Count(); i++)
            {
                sprites[i].Draw();
            }

            //draw the buttons
            for (int i = 0; i < buttonsList.Count(); i++)
            {
                buttonsList[i].Draw();
            }
        }


    }

}
