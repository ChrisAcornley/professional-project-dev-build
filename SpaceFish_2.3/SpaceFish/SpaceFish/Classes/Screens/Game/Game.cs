#region Includes
using System;

using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Runtime;
using System.Runtime.Serialization;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using SpaceFish.Sprite;
using SpaceFish.Button;
using SpaceFish;
using SpaceFish.Screen;
#endregion

namespace SpaceFish.Screen
{
    public class GameClass : ScreenClass
    {
        //list of the worlds
        List<world> theWorlds;
        
        //array for the road
        public string[,] road = new string[25, 15];
        
        //INDICATOR OF WHAT WORLD AND ROAD IS TO BE LOADED
        int world;
        int level;
        

        Texture2D tilesheet;
        ContentManager ContentM;
        ButtonManager BM;
        InputHandler inputHandler;
        PathClass pathSelecting;
        //State Machine
        public enum GameState { SELECTINGPATH, PLAYING, CONTEXMENU, PAUSED }
        public GameState theGameState;

        public GameClass(ContentManager Contentm , List<world> Worlds, ScreenManager ScreenM,InputHandler inputH)
        {
            this.inputHandler = inputH;
            //pass the content manager though
            ContentM = Contentm;
            //pass the list of worlds though
            theWorlds = Worlds;
            BM = new Button.ButtonManager(ScreenM);
            pathSelecting = new PathClass();
        }

        void InitialiseLevel(){

            towerList.Clear();  //clear the tower list for a new level
            sprites.Clear();    //clear the sprite list for a new level
            buttonsList.Clear();//clear the button list for a new level
            BuildLevel();       //build the level  
            pathSelecting.ClearSprites();
            pathSelecting.InitialisePath(road, ContentM);
        }

        public void WhichLevel(int whichWorld, int whichLevel)
        {

            world = whichWorld;
            level = whichLevel;
            InitialiseLevel();
        }

        public void Update(GameTime gametime)
        {   
            //check to see if its in the path select state
            if (theGameState == GameState.SELECTINGPATH)
            {
                //if the screen is being pressed
                if (inputHandler.mouseState.LeftButton == ButtonState.Pressed)
                {
                    //check if the user can draw here and draw & block paths acordingly
                    pathSelecting.CanIDrawHere(inputHandler.mouseState.X, inputHandler.mouseState.Y);
                }
                road =  pathSelecting.ReturnList();

                // loop through the list of sprites and update them
                for (int i = 0; i < sprites.Count(); i++)
                {
                    sprites[i].Update(gametime);
                }

                for (int i = 0; i < towerList.Count(); i++)
                {
                    towerList[i].Update(gametime);
                }
            }

            // loop through the list of sprites and update them
            for (int i = 0; i < sprites.Count(); i++)
            {
                sprites[i].Update(gametime);
            }

            for (int i = 0; i < towerList.Count(); i++)
            {
                towerList[i].Update(gametime);
            }
            
        }
        
        public void Draw()
        {

            
            // loop through the list of sprites and update them
            for (int i = 0; i < sprites.Count(); i++)
            {
                sprites[i].Draw();
            }
            for (int i = 0; i < towerList.Count(); i++)
            {
                towerList[i].Draw();
            }
            pathSelecting.draw();
        }

        void BuildLevel()
        {
            for (int x = 0; x < 25; x++) //each column
            {
               
            }
            tilesheet = ContentM.Load<Texture2D>(theWorlds[world].spriteSheetFilePath);
            for (int i = 0; i < 5; i++) //each layer
            {                 
                for (int x = 0; x < 25; x++) //each column
                {           
                    for (int y = 0; y < 15; y++)//each row
                    {
                        //get the tiles road values
                        road[x, y] = theWorlds[world].levelobj[level].layerobj[2].tileobj[x, y].indicator;
                        foreach (Item itemtemp in theWorlds[world].items)//go though the list of current existing items
                        {
                           if (itemtemp.name == (string)theWorlds[world].levelobj[level].layerobj[i].tileobj[x,y].name) // if the tile we are on matches one of the existing items
                            {
                                //if its a type tower
                                if (itemtemp.type == "tower")//check if the item selected is of type tower
                                {
                                    //if it is then add a tower at that position of the specific tower type on that tile
                                    towerList.Add(new TowerClass(tilesheet,                                                                                                     //spritesheet
                                                                    new Vector2(x * 32, y * 32),                                                                                                  //position
                                                                    itemtemp.spriterect,                                                                                                          //position on spritesheet for tower
                                                                    theWorlds[world].towers.Where(theItem => theItem.name == itemtemp.name).Select(attr => attr.projectileRect).Single(),     //position on spritesheet for projectile
                                                                    theWorlds[world].towers.Where(theItem => theItem.name == itemtemp.name).Select(attr => attr.health).Single(),             //health
                                                                    theWorlds[world].towers.Where(theItem => theItem.name == itemtemp.name).Select(attr => attr.range).Single(),              //range
                                                                    theWorlds[world].towers.Where(theItem => theItem.name == itemtemp.name).Select(attr => attr.damage).Single(),             //damage
                                                                    theWorlds[world].towers.Where(theItem => theItem.name == itemtemp.name).Select(attr => attr.rateOfFire).Single(),         //rate of fire
                                                                    theWorlds[world].towers.Where(theItem => theItem.name == itemtemp.name).Select(attr => attr.name).Single()                //name
                                                                    ));
                                }//if its a road
                                else if (itemtemp.type == "road")
                                {
                                    
                                    // add it to the list of sprites to draw
                                    sprites.Add(new SpriteClass(tilesheet,
                                    new Vector2(x * 32, y * 32),
                                    itemtemp.spriterect,
                                    true));
                                }
                                    //if background or scenery
                                else
                                {
                                    // add it to the list of sprites to draw
                                    sprites.Add(new SpriteClass(tilesheet,
                                    new Vector2(x * 32, y * 32),
                                    itemtemp.spriterect,
                                    true));
                                }
                            }//end type check
                        }//end foreach item loop
                    }//end row
                }//end column
            }//end layer
        }//end draw_level function

        private void units()
        {
            
        }


    }//end of class
}//end of namespace
