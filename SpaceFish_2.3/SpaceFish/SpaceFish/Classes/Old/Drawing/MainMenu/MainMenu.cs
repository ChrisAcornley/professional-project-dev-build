#region Includes
using System;

using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Runtime;
using System.Runtime.Serialization;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
#endregion

namespace TowerAttack.Sprite
{
    public class MainMenu
    {
        //Declaring buttons textures
            Texture2D Backgroundtex;
        //Declar the position of hud
            Vector2   BackgroundPosition;
        //Declaring the playbuttons for use
            Texture2D Button1tex;
            Vector2   Button1Position;
        //Declare a list of the buttons from class
            List<Sprite> sprites = new List<Sprite>();    

        //AUDIO CODE
            private bool isMusicPlaying = false;
            Song SongMenu;
            List<Song> SongList = new List<Song>();
            int CurrentSong;

        public MainMenu(ContentManager Content)
        {
            //Texture data for the menu
            Backgroundtex = Content.Load<Texture2D>("Content/Graphics/MainMenu/Background");
            //Initialise the position
            BackgroundPosition = new Vector2((int)Game1.screensize.X - (int)Game1.screensize.X, (int)Game1.screensize.Y - (int)Game1.screensize.Y);
            //Texture data for the menu
            Button1tex = Content.Load<Texture2D>("Content/Graphics/MainMenu/Start"); ;
            //inialise the position of the button
            Button1Position = new Vector2(BackgroundPosition.X + 50, BackgroundPosition.Y +400 ); 
         

            sprites.Add(new Background(Backgroundtex, new Vector2(BackgroundPosition.X, BackgroundPosition.Y)));
            sprites.Add(new PlayButton(Button1tex, new Vector2(Button1Position.X, Button1Position.Y)));


            SongMenu = Content.Load<Song>("Content/Audio/MainMenu/SpaceMenu");
            SongList.Add(SongMenu);


        }
        public void Update()
        {

            // loop through the list
            for (int i = 0; i < 2; i++)
            {
                sprites[i].Update();

                //checking for interaction

                if (sprites[i].Clicked)
                    sprites[i].Effect();
            }

            //if the track is not currently playing or paused, play song
            if (isMusicPlaying == false && Game1.gameState == Game1.GameState.StartMenu)
            {
                MediaPlayer.Play(SongMenu);
                isMusicPlaying = true;
            }
            else if (Game1.gameState == Game1.GameState.Playing)
            {
                MediaPlayer.Stop();
            }


        }
        public void Draw()
        {

            for (int i = 0; i < 2; i++)
            {
                sprites[i].Draw();
            }
        }

    }

}
