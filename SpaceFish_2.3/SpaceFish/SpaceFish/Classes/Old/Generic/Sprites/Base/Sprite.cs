#region includes
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Audio;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.GamerServices;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Media;
#endregion


namespace TowerAttack.Sprite
{
    public class Sprite
    {
        #region declariations
        //making the sprite clickable
        protected bool isClickable = false;
        public bool Clicked;
        public bool lastClicked = false;
        bool isOn = false;

        //texture
        Texture2D texture;
        //position for the sprite
        public Vector2 position;
        //hitbox
        Rectangle collisionRect;
        #endregion

        //Constructor that passses in two variables
        public Sprite(Texture2D tex, Vector2 position)
        {
            //Updating the variables
            this.texture = tex;
            this.position = position;

        }

        //polymorphic function
        public virtual void Update()
        {
            //Gettint eh Collision from the buttons
            collisionRect = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
            //getting the mouse pos
            MouseState mouse = Mouse.GetState();
            Rectangle mousePos = new Rectangle(mouse.X, mouse.Y, 1, 1);
            //Check if the cursor is over button
            if (mousePos.Intersects(collisionRect) && isClickable == true)
                isOn = true;
            else
                isOn = false;
            //Checking if the button is Clicked
            if (mousePos.Intersects(collisionRect) && mouse.LeftButton == ButtonState.Pressed && !lastClicked)
                Clicked = true;
            else
                Clicked = false;
            //Updating the last Clicked
            lastClicked = lastClicked || Clicked;
        }

        //polymorphic function
        public virtual void Effect()
        {
            //Overridden

        }

        public void Draw()
        {
            //Draw the button
            if (isOn && isClickable == true)
            {

                Game1.spriteBatch.Draw(texture, position, new Color(255, 0, 0, 180));
            }
            else
                Game1.spriteBatch.Draw(texture, position, Color.White);
        }
    }
}

