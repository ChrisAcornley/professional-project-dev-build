using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace  TowerAttack.Sprite
{
    class Background : Sprite
    {
        //Declare an instance variable
        public bool Click = false;

        //Constructor
        public Background(Texture2D texture, Vector2 position)
            : base(texture, position)
        {

        }

        //Update Clicked
        public override void Update()
        {
            Click = base.Clicked;
            base.Update();
        }


        //Event for the button
        public override void Effect()
        {
            base.lastClicked = false;

            base.Effect();
        }
    }
}
