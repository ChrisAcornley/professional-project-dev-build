using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceFish
{
    public class Tile
    {
        public string name;
        public string type;
        public string indicator;

        public Tile()
        {
            name = "";
            type = "";
            indicator = "";
        }
    }
}
