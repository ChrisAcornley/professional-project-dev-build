using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceFish
{
    public class world
    {

        public level[]  levelobj    { get; set; }
        public Item[]   items       { get; set; }
        public Towers[] towers      { get; set; }
        public Units[]  units       { get; set; }
        public Road[]   roads       { get; set; }

        public string spriteSheetFilePath;


        public world(int noOfLevels, int noOfItems,int noOfTowers, int noOfUnits,int noOfRoads)
        {
            //when creating a world make the levels
            levelobj = new level[noOfLevels];
            items = new Item[noOfItems];
            towers = new Towers[noOfTowers];
            units = new Units[noOfUnits];
            roads = new Road[noOfRoads];

            for (int i = 0; i < noOfItems; i++)
            {
                items[i] = new Item();
            }
            for(int i =0; i< noOfLevels; i++)
            {
                levelobj[i] = new level();
            }

            for (int i = 0; i < noOfTowers; i++)
            {
                towers[i] = new Towers();
            }
            for (int i = 0; i < noOfUnits; i++)
            {
                units[i] = new Units();
            }

            for (int i = 0; i < noOfRoads; i++)
            {
                roads[i] = new Road();
            }
            
        }        
    }
}
