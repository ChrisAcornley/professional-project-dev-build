using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpaceFish
{
    public class Towers
    {
        //the rectangle on the sprite sheet to draw
        public Rectangle spriteRect;

        //the rectangle to draw for the projectile
        public Rectangle projectileRect;

        //its type
        public string type;
        //its name
        public string name;
        //its damage 
        public float damage;
        //its speed
        public float rateOfFire;
        //its health
        public float health;
        //its range
        public float range;

        public Towers()
        {
            spriteRect.X = 0;
            spriteRect.Y = 0;
            spriteRect.Width = 0;
            spriteRect.Height = 0;

            projectileRect.X = 0;
            projectileRect.Y = 0;
            projectileRect.Width = 0;
            projectileRect.Height = 0;

            name = "default";
            type = "default";
            rateOfFire = 0;
            health = 0;
            range = 0;
            damage = 0;
        }
    }
}
