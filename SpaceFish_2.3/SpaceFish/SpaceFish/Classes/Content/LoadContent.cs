using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Diagnostics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceFish.Contents
{
    public class LoadContent
    {
        //SET THE STANDARD SIZES FOR THE GAME
        int tile_size;
        int rows;
        int columns;

        //holder for the XML file
        XDocument levelData;
        public List<world> Worlds;
        public List<IEnumerable<XElement>> worldList;
        public List<IEnumerable<XElement>> worldLevels;

        //constructor
        public LoadContent(ContentManager Content)
        {
            
            //load the xml file in
            levelData = XDocument.Load("LevelData.xml");
            Worlds = new List<world>(); 
            worldList = new List<IEnumerable<XElement>>();    
            worldLevels = new List<IEnumerable<XElement>>();
            //SET THE STANDARD SIZES FOR THE GAME
            tile_size = (int)levelData.Element("worlds").Attribute("tilesize");
            rows = (int)levelData.Element("worlds").Attribute("width");
            columns = (int)levelData.Element("worlds").Attribute("height");
            
            LoadLevelData();

        }

        public void LoadLevelData(){
            //counters
            int levelCounter = 0;
            int worldCounter = 0;
            int layerCounter = 0;

            int worldCounterForItems = 0;


            //gets the number of worlds in the xml file
            IEnumerable<XElement> theWorlds =
            from element in levelData.Root.DescendantsAndSelf("world")  //check each element in leveData and if its name is world select this element and 
            select element;                                             //its decendants up to the next element called world and add it to theWorlds as a single entry
            int noOfWorlds = theWorlds.Count();                         //count the number of entries in theWorlds
            
            //for each world
            for (int i = 0; i < noOfWorlds; i++)
            {
                //add the information about the current world in the form of a list to the worldslist
                worldList.Add(levelData.Root.Elements("world").Where(element => (int)element.Attribute("id") == ((i + 1))).ToList());
            }

            //for each world in WorldsList
            foreach (IEnumerable<XElement> world in worldList)
            {
                //gets the number of levels in each world
                IEnumerable<XElement> theLevels =
               from element in world.DescendantsAndSelf("level")
               select element;
                int nooflevels = theLevels.Count();

                IEnumerable<XElement> Items =
                    from element in world.DescendantsAndSelf("item")
                    select element;

                //count the number of items associated with that world
                IEnumerable<XElement> theItems =
                    from element in world.DescendantsAndSelf("item")
                    select element;
                int numberofItems = theItems.Count();


                //count the number of items associated with that world
                IEnumerable<XElement> theRoads =
                    from element in theItems.DescendantsAndSelf("item").Where(theTower => (string)theTower.Attribute("itemtype") == "road").ToList()
                    select element;
                int numberOfRoads = theRoads.Count();

                //count the number of towers associated with that world
                IEnumerable<XElement> theTowers =
                    from element in theItems.DescendantsAndSelf("item").Where(theTower => (string)theTower.Attribute("itemtype") == "tower").ToList()
                    select element;
                int numberofTowers = theTowers.Count();

                //count the number of units associated with that world
                IEnumerable<XElement> theUnits =
                    from element in theItems.DescendantsAndSelf("item").Where(theUnit => (string)theUnit.Attribute("itemtype") == "unit").ToList()
                    select element;
                int numberofUnits = theUnits.Count();

                //grab the spritesheet fvor each world
                columns = (int)levelData.Element("worlds").Attribute("height");

                worldLevels.Add(theLevels);
                //create the world with number of levels
                world temp = new world(nooflevels, numberofItems, numberofTowers, numberofUnits,numberOfRoads);
                //add the temp into the list of worlds
                Worlds.Add(temp);

                //add the road items associataed to that world to the associated world
                int itemCounter = 0;
                foreach (XElement item in theItems)
                {
                    Worlds[worldCounterForItems].items[itemCounter].spriterect.X = (int)item.Attribute("sourcex");
                    Worlds[worldCounterForItems].items[itemCounter].spriterect.Y = (int)item.Attribute("sourcey");
                    Worlds[worldCounterForItems].items[itemCounter].spriterect.Width = (int)item.Attribute("sizex");
                    Worlds[worldCounterForItems].items[itemCounter].spriterect.Height = (int)item.Attribute("sizey");
                    Worlds[worldCounterForItems].items[itemCounter].name = (string)item.Attribute("name");
                    Worlds[worldCounterForItems].items[itemCounter].type = (string)item.Attribute("itemtype");
                    itemCounter++;
                }
                itemCounter = 0;

                int roadCounter = 0;
                foreach (XElement road in theRoads)
                {
                    Worlds[worldCounterForItems].roads[roadCounter].spriterect.X = (int)road.Attribute("sourcex");
                    Worlds[worldCounterForItems].roads[roadCounter].spriterect.Y = (int)road.Attribute("sourcey");
                    Worlds[worldCounterForItems].roads[roadCounter].spriterect.Width = (int)road.Attribute("sizex");
                    Worlds[worldCounterForItems].roads[roadCounter].spriterect.Height = (int)road.Attribute("sizey");
                    Worlds[worldCounterForItems].roads[roadCounter].name = (string)road.Attribute("name");
                    Worlds[worldCounterForItems].roads[roadCounter].type = (string)road.Attribute("itemtype");
                    itemCounter++;
                }
                roadCounter = 0;

                //add the tower items associataed to that world to the associated world
                int towerCounter = 0;
                foreach (XElement tower in theTowers)
                {
                    //part of spritesheet to draw for the tower
                    Worlds[worldCounterForItems].towers[towerCounter].spriteRect.X = (int)tower.Attribute("sourcex");
                    Worlds[worldCounterForItems].towers[towerCounter].spriteRect.Y = (int)tower.Attribute("sourcey");
                    Worlds[worldCounterForItems].towers[towerCounter].spriteRect.Width = (int)tower.Attribute("sizex");
                    Worlds[worldCounterForItems].towers[towerCounter].spriteRect.Height = (int)tower.Attribute("sizey");
                    //same again for the projectile
                    Worlds[worldCounterForItems].towers[towerCounter].projectileRect.X = (int)tower.Attribute("sourcex");
                    Worlds[worldCounterForItems].towers[towerCounter].projectileRect.Y = (int)tower.Attribute("sourcey");
                    Worlds[worldCounterForItems].towers[towerCounter].projectileRect.Width = (int)tower.Attribute("sizex");
                    Worlds[worldCounterForItems].towers[towerCounter].projectileRect.Height = (int)tower.Attribute("sizey");
                    //other info
                    Worlds[worldCounterForItems].towers[towerCounter].type = (string)tower.Attribute("itemtype");
                    Worlds[worldCounterForItems].towers[towerCounter].name = (string)tower.Attribute("name");
                    Worlds[worldCounterForItems].towers[towerCounter].health = (float)tower.Attribute("health");
                    Worlds[worldCounterForItems].towers[towerCounter].rateOfFire = (float)tower.Attribute("rateoffire");
                    Worlds[worldCounterForItems].towers[towerCounter].range = (float)tower.Attribute("range");
                    Worlds[worldCounterForItems].towers[towerCounter].damage = (float)tower.Attribute("damage");

                    towerCounter++;
                }
                towerCounter = 0;

                //add the unit items associataed to that world to the associated world
                int unitCounter = 0;
                foreach (XElement unit in theUnits)
                {
                    Worlds[worldCounterForItems].units[unitCounter].spriteRect.X = (int)unit.Attribute("sourcex");
                    Worlds[worldCounterForItems].units[unitCounter].spriteRect.Y = (int)unit.Attribute("sourcey");
                    Worlds[worldCounterForItems].units[unitCounter].spriteRect.Width = (int)unit.Attribute("sizex");
                    Worlds[worldCounterForItems].units[unitCounter].spriteRect.Height = (int)unit.Attribute("sizey");
                    Worlds[worldCounterForItems].units[unitCounter].name = (string)unit.Attribute("name");
                    Worlds[worldCounterForItems].units[unitCounter].type = (string)unit.Attribute("itemtype");
                    Worlds[worldCounterForItems].units[unitCounter].health = (float)unit.Attribute("health");
                    Worlds[worldCounterForItems].units[unitCounter].speed = (float)unit.Attribute("speed");
                    Worlds[worldCounterForItems].units[unitCounter].rateOfFire = (float)unit.Attribute("rateoffire");
                    Worlds[worldCounterForItems].units[unitCounter].range = (float)unit.Attribute("range");
                    Worlds[worldCounterForItems].units[unitCounter].damage = (float)unit.Attribute("damage");
                    unitCounter++;
                }
                unitCounter = 0;

                //do the same for the spritesheet for that world
                foreach(XElement element in world)
                {
                    Worlds[worldCounterForItems].spriteSheetFilePath = (string)element.Element("tilesheet").Attribute("source");
                }
                //add 1 to the counter for each word in this loop
                worldCounterForItems++;
            }//end loop



            //for each world
            foreach (IEnumerable<XElement> WORLD in worldLevels)
            {
                //get each world as a temp
                IEnumerable<XElement> tempworld =
                    from element in WORLD.DescendantsAndSelf("level") // next all elements with name level
                    select element;

                Debug.WriteLine("this is World: " + worldCounter);

                //for each level in the world
                foreach (XElement LEVEL in tempworld.DescendantsAndSelf("level").Where(temp => temp.Element("level") == temp.Element("level")))
                {
                    Debug.WriteLine("This is level: " + levelCounter);
                    //get the levels as a temp
                    IEnumerable<XElement> templevel =
                                        from element in LEVEL.DescendantsAndSelf("layer") // next all elements with name level
                                        select element;
                    //for each layer in the temp levels
                    foreach(XElement LAYER in templevel.DescendantsAndSelf("layer").Where(temp => temp.Element("layer") == temp.Element("layer")))
                    {
                        //get the layers as a temp
                        IEnumerable<XElement> templayer =
                                        from element in LAYER.DescendantsAndSelf("tile") // next all elements with name level
                                        select element;
                        //for each tile in that level
                        foreach (XElement temptile in templayer.DescendantsAndSelf("tile"))
                        {
                            Worlds[worldCounter].levelobj[levelCounter].layerobj[layerCounter].tileobj[(int)temptile.Attribute("x"), (int)temptile.Attribute("y")].name = (string)temptile.Attribute("name");
                            Worlds[worldCounter].levelobj[levelCounter].layerobj[layerCounter].tileobj[(int)temptile.Attribute("x"), (int)temptile.Attribute("y")].type = (string)temptile.Attribute("type");
                            Worlds[worldCounter].levelobj[levelCounter].layerobj[layerCounter].tileobj[(int)temptile.Attribute("x"), (int)temptile.Attribute("y")].indicator = (string)temptile.Attribute("indicator");
                        }//end tile
                        Debug.WriteLine("layer: " + layerCounter);
                        layerCounter++;
                    }//end layer
                    layerCounter = 0;
                    levelCounter++;
                }//end level
                levelCounter = 0;
                worldCounter++;
            }//end world
        }
    }
}
