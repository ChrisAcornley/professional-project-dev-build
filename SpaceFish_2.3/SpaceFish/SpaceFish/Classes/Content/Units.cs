using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpaceFish
{
    public class Units
    {
        //the rectangle on the sprite sheet to draw
        public Rectangle spriteRect;

        //its name
        public string name;
        //its type
        public string type;
        //its rate of fire
        public float rateOfFire;
        //its damage
        public float damage;
        //its speed
        public float speed;
        //its health
        public float health;
        //its range
        public float range;

        public Units()
        {
            spriteRect.X = 0;
            spriteRect.Y = 0;
            spriteRect.Width = 0;
            spriteRect.Height = 0;
            name = "default";
            speed = 0;
            health = 0;
            range = 0;
        }
    }
}
