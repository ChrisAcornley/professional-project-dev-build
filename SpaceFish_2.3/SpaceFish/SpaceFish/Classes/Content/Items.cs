using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpaceFish
{
    public class Item
    {
        public Rectangle spriterect;
        public string name { get; set; }
        public string type { get; set; }

        public Item()
        {
            spriterect.X = 0;
            spriterect.Y = 0;
            spriterect.Width = 0;
            spriterect.Height = 0;
            name = "default";
            type = "default";
        }

    }
}
