using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceFish
{
    public class layer
    {

        public Tile[,] tileobj { get; set; }

        public layer()
        {
            //when creating a world make the levels
            tileobj = new Tile[25,15];

            for (int i = 0; i < 25; i++)
            {
                for (int j = 0; j < 15; j++)
                {
                    tileobj[i, j] = new Tile();
                }
            }
        }

        
        
    }
}
