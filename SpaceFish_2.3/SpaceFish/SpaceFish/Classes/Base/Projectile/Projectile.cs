using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using SpaceFish.Sprite;
using Microsoft.Xna.Framework;
using SpaceFish;

namespace SpaceFish.Projectile
{
    class ProjectileClass : SpriteClass
    {
        //projectile variables
        float projectileSpeed;
        Vector2 projectilePosition;
       
        //target variables
        UnitClass unitTarget;
        TowerClass towerTarget;
        bool isTargetATower;

        //if made by a tower
        public ProjectileClass(Texture2D tex,Vector2 startingPos, Rectangle rect, float speed,float damage, UnitClass closestUnit)
            : base(tex, startingPos,rect,false)
        {
            this.projectileSpeed = speed;
            this.unitTarget = closestUnit;
            this.isTargetATower = false;
        }

        //if made by a unit
        public ProjectileClass(Texture2D tex, Vector2 StartingPos, Rectangle rect, float speed, float damage, TowerClass theTower)
            : base(tex,StartingPos,rect,false)
        {
            this.projectileSpeed = speed;
            this.towerTarget = theTower;
            this.isTargetATower = true;
        }

        
        public override void Draw()
        {
            //set the position to draw to the current position then draw
            position = projectilePosition;
            base.Draw();
        }


        //movement for the projectile
        public void Movement()
        {
            if (isTargetATower)
            {
                //get the distance from the target to target 
                Vector2 directionToTarget = towerTarget.position - projectilePosition;

                //move towards target
                projectilePosition += directionToTarget * projectileSpeed;
            }
            else
            {
                //direction to target in form of Vector2
                Vector2 directionToTarget = unitTarget.position - projectilePosition;

                //move towards target
                projectilePosition += directionToTarget;
            }
        }

        //check to see if the projectile has collided
        public bool Collided()
        {
            //check what tipe of projectile it is
            if (isTargetATower)
            {
                //check if it has collided with the target tower
                if (CollidedWith(towerTarget))
                {
                    //is it has return true for collision
                    return true;
                }//else do nothing
            }
            else
            {
                //check if it has collided with the target unit
                if (CollidedWith(unitTarget))
                {
                    //is it has then return true for collision
                    return true;
                }//else do nothing
            }
            //return false if no collision has occured
            return false;
        }


        //check if the projectile has collided with the unit
        bool CollidedWith(UnitClass unitTemp)
        {
            //if the squared distance between the two points is less then or equal to the radii added together
            if (Vector2.Distance(this.position, unitTemp.position) <= (2.0f + 2.0f))
            {
                //collided
                return true;
            }

            //did not collide
            return false;
        }

        //check if the projectile has collided with the tower
        bool CollidedWith(TowerClass towerTemp)
        {
            //if the squared distance between the two points is less than or equal to the radii added together
            if (Vector2.Distance(this.position, towerTemp.position) <= (2.0f + 2.0f))
            {
                //collided
                return true;
            }

            //did not collide
            return false;
        }
    }
}
