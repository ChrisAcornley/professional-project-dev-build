using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpaceFish.Sprite;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using SpaceFish.Projectile;

namespace SpaceFish
{
    class UnitClass: SpriteClass
    {
        //tower variables
        float unitHealth;
        float unitAttackRange;
        float unitRateOfFire;
        float unitdamage;
        float unitSpeed;
        string unitName;
        Vector2 currentPosition;
        
        float totalElapsedTime;

        //projectile initialisation stoof
        string projectileType;
        List<ProjectileClass> projectiles;
        Texture2D projectileTex;
        Rectangle projectileRect;
        float projectileSpeed;

        public UnitClass(Texture2D texture, Vector2 position, Rectangle unitRect,Rectangle projectileRect,  float health, float range, float damage, float speed,float rateoffire, string unitType)
            : base(texture, position, unitRect, false)
        {
            //projectile variables
            this.projectileRect = projectileRect;
            this.projectileTex = texture;
            

            //tower variables
            this.currentPosition = position;
            this.unitRateOfFire = rateoffire;
            this.unitHealth = health;
            this.unitAttackRange = range;
            this.unitdamage = damage;
            this.unitSpeed = speed;
            this.unitName = unitType;
        }

         public override void Update(GameTime gt)
        {
            //get the total elapsed time
            totalElapsedTime += (float)gt.ElapsedGameTime.TotalSeconds;


            //check to see if any tower is in range if so then fire apon it!
            InRange();

            //update each of the projectiles
            foreach (ProjectileClass projectile in projectiles)
            {
                projectile.Movement();
                //if the current bullet has reached the target
                if (projectile.Collided())
                {
                    //remove the current bullet
                    projectiles.Remove(projectiles.First());
                }
            }
            base.Update(gt);
        }

        public override void Draw()
        {
            //draw each projectile
            foreach (ProjectileClass projectile in projectiles)
            {
                projectile.Draw();
            }

            base.Draw();
        }

        void InRange()
        {
            //TEMP!!!!!
            List<TowerClass> Towers = new List<TowerClass>();//to be deleted (represents the list of units from the game)
            //TEMP!!!!!

            // list of all units in range
            List<TowerClass> listOfTowersInRange = new List<TowerClass>(); 
            //the closest unit
            TowerClass closestTower = null;
            //the closest units distance to the tower
            float clostestUnitDistance = unitAttackRange;
            
            

            //check to see if one of the units are in the radious 'range'


            //for each unit in the list of units
            foreach(TowerClass towerInRange in Towers)
            {
                //check if they are within range and if so add them to a list
                if (Vector2.DistanceSquared(this.position, towerInRange.position) <= (this.unitAttackRange + 1.0f))
                {
                    listOfTowersInRange.Add(towerInRange);
                }
            
            }

            //go through the list of units in range and
            foreach (TowerClass tower in listOfTowersInRange)
            {
                //get the distance of the current unit to the tower
                float tempDistance = Vector2.Distance(currentPosition, tower.position);
                
                //if that is less than the current closest unit
                if (tempDistance < clostestUnitDistance)
                {
                    //set the closest unit to the temp as its now the closest
                    clostestUnitDistance = tempDistance;
                    //and take a copy of the current closest unit
                    closestTower = tower;
                }
            }

            //once the closest unit has been calculated fire at it
            if (closestTower != null)
            {
                Fire(closestTower);
            }
            
        }

        void Fire(TowerClass theclosestTower)
        {
            if (totalElapsedTime > unitRateOfFire)
            {
                projectiles.Add(new ProjectileClass(projectileTex, currentPosition,projectileRect, unitRateOfFire, unitdamage, theclosestTower));
                totalElapsedTime -= totalElapsedTime;
            }
        }
    }
}

