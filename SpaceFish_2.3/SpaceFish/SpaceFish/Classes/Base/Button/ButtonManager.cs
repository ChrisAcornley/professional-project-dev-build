using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using SpaceFish.Sprite;
using SpaceFish.Screen;
namespace SpaceFish.Button
{
    //subscriber to each button
    class ButtonManager :ScreenClass
    {
        ScreenManager ScreenManagr;
        public ButtonManager(ScreenManager SM)
        {
            ScreenManagr = SM;
        }

        //given the button, subscribe to its buttonpressedhandlerevent
        public void Subscribe(ButtonClass theButton)
        {
            theButton.ButtonPressedDelegate +=
                new ButtonClass.ButtonPressedHandler(ButtonHasBeenPressed);
        }

        public void ButtonHasBeenPressed(object theButton, PressedButton buttonInfo)
        {
            //function that deals with what function to be used
            buttonfunctionality(buttonInfo.buttonindicator, ScreenManagr);
        }


    }
}
