using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using SpaceFish.Sprite;

namespace SpaceFish.Button
{
    //container class
    public class PressedButton
    {
        public int buttonindicator;

        public PressedButton(int buttonpressed)
        {
            this.buttonindicator = buttonpressed;
        }
    }

    //button class
    public class ButtonClass : SpriteClass
    {
        //Declare an instance variable
        int thisButtonIndicator;

        //the delegate the subscribers must implement
        public delegate void ButtonPressedHandler(object Button, PressedButton pressedButton);
        //an instance of said delegate
        public ButtonPressedHandler ButtonPressedDelegate;
        //create the buttonpressed object
        PressedButton pressedButton;

        //Constructor
        public ButtonClass(Texture2D texture, Vector2 position , int buttonIndicator)
            : base(texture, position, false)
        {
            this.thisButtonIndicator = buttonIndicator;
            pressedButton = new PressedButton(thisButtonIndicator);

        }

        //update function
        public override void Update(GameTime gt)
        {
            //if clicked
            if (ClickedDetection())
            {
                //if anyone has subscribed, notify them
                if (ButtonPressedDelegate != null)
                {
                    ButtonPressedDelegate(this, pressedButton);
                }
            }
            base.Update(gt);
        }
    }
}
