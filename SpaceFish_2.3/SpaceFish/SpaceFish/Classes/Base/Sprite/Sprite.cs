#region includes
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Audio;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.GamerServices;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Media;
#endregion


namespace SpaceFish.Sprite
{
    public class SpriteClass
    {
        #region declariations
        //making the sprite clickable
        protected bool isClickable = false;
        protected bool isbutton = false;
        public bool Clicked;
        public bool amIBeingPressed = false;
        public bool lastClicked = false;
       
        bool doClickDetection = false;
        bool isStaticTex;
        //texture
        Texture2D texture;
        //position for the sprite
        public Vector2 position;
        //hitbox
        Rectangle collisionRect;

        Rectangle spriteRect;
        #endregion

        //Constructor that passses in three variables
        public SpriteClass(Texture2D tex, Vector2 position, bool clickDetection)
        {
            //Updating the variables
            this.texture = tex;
            this.position = position;
            this.doClickDetection = clickDetection;
            this.isStaticTex = true;
        }

        //Constructor that passses in four variables
        public SpriteClass(Texture2D tex, Vector2 position,Rectangle rect, bool clickDetection)
        {
            //Updating the variables
            this.texture = tex;
            this.position = position;
            this.doClickDetection = clickDetection;
            this.spriteRect = rect;
            this.isStaticTex = false;
        }

        public SpriteClass(Texture2D tex)
        {
            this.texture = tex;
        }

        //polymorphic function
        public virtual void Update(GameTime gt)
        {
            if (doClickDetection)
            {
                ClickedDetection();
            }
        }

        //polymorphic function
        public virtual void Effect()
        {
            //Overridden

        }

        public virtual void Draw()
        {
            if (isStaticTex)
            {
                Main.spriteBatch.Draw(texture, position, Color.White);
            }
            else
            {
                Main.spriteBatch.Draw(texture, position, spriteRect, Color.White);              
            }
        }

        

        public bool ClickedDetection()
        {
            //Gettint eh Collision from the buttons
            collisionRect = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
            //getting the mouse pos
            MouseState mouse = Mouse.GetState();
            Rectangle mousePos = new Rectangle(mouse.X, mouse.Y, 1, 1);


            //Check if the cursor is over button
            if (mousePos.Intersects(collisionRect) && isClickable == true)
            {
                //isOn = true;
            }
            else
            {
               // isOn = false;
            }

            if (mousePos.Intersects(collisionRect) && mouse.LeftButton == ButtonState.Released && amIBeingPressed)
            {
                amIBeingPressed = false;
            }
            

            //Checking if the button is Clicked
            if (mousePos.Intersects(collisionRect) && mouse.LeftButton == ButtonState.Pressed && !amIBeingPressed)
            {
                amIBeingPressed = true;
                return true;
            }
            else
            {
                
                return false;
            }

        }
    }
}

