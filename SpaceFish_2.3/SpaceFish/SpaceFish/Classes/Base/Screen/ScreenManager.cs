using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpaceFish;
using Microsoft.Xna.Framework.Content;

namespace SpaceFish.Screen
{
    public class ScreenManager
    {
        //declare the MainMenu for use (with its get andset functions
        public Screen.MainMenuClass MainMenuObj { get; set; }

        //same for the game
        public Screen.GameClass GameObj { get; set; }

        //same for the loader
        public Contents.LoadContent Loaderobj { get; set; }
        
        
        public ScreenManager(ContentManager Content,InputHandler iH)
        {
            //initialise each of these for use
            MainMenuObj = new Screen.MainMenuClass(Content,this);
            Loaderobj = new Contents.LoadContent(Content);
            GameObj = new Screen.GameClass(Content, Loaderobj.Worlds, this,iH);
        }


    }
}
