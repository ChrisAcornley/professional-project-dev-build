using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpaceFish.Sprite;
using System.Diagnostics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using SpaceFish.Button;

namespace SpaceFish.Screen
{
    public class ScreenClass
    {
        //instance of the screenmanager passed back to this parent
        //private ScreenManager ScreenManag;
        //Declare a list of the sprites from class
        public List<SpriteClass> sprites = new List<SpriteClass>();

        //declair a list of buttons 
        public List<ButtonClass> buttonsList = new List<ButtonClass>();

        public List<TowerClass> towerList = new List<TowerClass>();

        //AUDIO CODE
        public bool isMusicPlaying = false;
        public Song SongMenu;
        public Song SongGame;
        public List<Song> SongList = new List<Song>();

         
        public ScreenClass()
        {
        }



        public void buttonfunctionality(int button,ScreenManager SM)
        {
            switch (button)
            {
                case 0:
                    SM.GameObj.WhichLevel(0, 0);
                    Main.gameState = Main.GameState.PLAYING;                  
                    Debug.WriteLine("Im Button 0");
                    break;
                case 1:
                    SM.GameObj.WhichLevel(0, 1);
                    Main.gameState = Main.GameState.PLAYING;  
                    Debug.WriteLine("Im Button 1");
                    break;
                case 2:
                    Debug.WriteLine("Im Button 2");
                    break;
            }
            
        }

    }
}
