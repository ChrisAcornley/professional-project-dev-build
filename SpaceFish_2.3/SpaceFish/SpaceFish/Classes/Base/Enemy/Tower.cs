/// Name: Enemy
/// requirements: sprite class
/// Namespace: SpaceFish.Enemy
/// 
/// Functionality: Is as the base class for each tower ad contains all functionality for the towers
///

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpaceFish.Sprite;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using SpaceFish.Projectile;

namespace SpaceFish
{
    public class TowerClass : SpriteClass
    {
        //tower variables
        Vector2 towerPos;
        float towerHealth;
        float towerAttackRange;
        float towerdamage;
        float rateOfFire;
        float totalElapsedTime = 0;
        string towerName;

        Texture2D spriteSheet;
        Rectangle projectileRectangle;
        //projectile initialisation stoof
        List<ProjectileClass> projectiles = new List<ProjectileClass>();

        //when creating an enemny pass in the:
        //texture
        //position
        //health
        //range
        //speed
        //damage
        //name
        public TowerClass(Texture2D texture, Vector2 position, Rectangle towerRect,Rectangle projectileRect,  float health, float range, float damage, float speed, string towerType)
            : base(texture, position, towerRect, false)
        {
            this.projectileRectangle = projectileRect;
            this.spriteSheet = texture;
            this.towerPos = position;
            this.towerHealth = health;
            this.towerdamage = damage;
            this.towerAttackRange = range;
            this.rateOfFire = speed;
            this.towerName = towerType;

        }

        public override void Update(GameTime gt)
        {
            //get the total elapsed time
            totalElapsedTime += (float)gt.ElapsedGameTime.TotalSeconds;


            //check to see if any unit is in range if so then fire apon it!
            InRange();

            //update each of the projectiles
            foreach (ProjectileClass projectile in projectiles)
            {
                projectile.Movement();
                //if the current bullet has reached the target
                if (projectile.Collided())
                {
                    //remove the current bullet
                    projectiles.Remove(projectiles.First());
                }
            }
            base.Update(gt);
        }

        public override void Draw()
        {
            //draw each projectile
            foreach (ProjectileClass projectile in projectiles)
            {
                projectile.Draw();
            }

            base.Draw();
        }

        void InRange()
        {
            //TEMP!!!!!
            List<UnitClass> Units = new List<UnitClass>();//to be deleted (represents the list of units from the game)
            //TEMP!!!!!

            // list of all units in range
            List<UnitClass> listOfUnitsInRange = new List<UnitClass>(); 
            //the closest unit
            UnitClass closestUnit = null;
            //the closest units distance to the tower
            float clostestUnitDistance = towerAttackRange;
            
            

            //check to see if one of the units are in the radious 'range'


            //for each unit in the list of units
            foreach(UnitClass unitInRange in Units)
            {
                //check if they are within range and if so add them to a list
                if (Vector2.DistanceSquared(this.position, unitInRange.position) <= (this.towerAttackRange + 1.0f))
                {
                    listOfUnitsInRange.Add(unitInRange);
                }
            
            }

            //go through the list of units in range and
            foreach (UnitClass unit in listOfUnitsInRange)
            {
                //get the distance of the current unit to the tower
                float tempDistance = Vector2.Distance(towerPos, unit.position);
                
                //if that is less than the current closest unit
                if (tempDistance < clostestUnitDistance)
                {
                    //set the closest unit to the temp as its now the closest
                    clostestUnitDistance = tempDistance;
                    //and take a copy of the current closest unit
                    closestUnit = unit;
                }
            }

            //once the closest unit has been calculated fire at it
            if (closestUnit != null)
            {
                Fire(closestUnit);
            }
        }

        void Fire(UnitClass theclosestUnit)
        {
            if (totalElapsedTime > rateOfFire)
            {
                projectiles.Add(new ProjectileClass(spriteSheet, towerPos,projectileRectangle , rateOfFire, towerdamage, theclosestUnit));
                totalElapsedTime -= totalElapsedTime;
            }
        }
    }
}
