using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using SpaceFish.Sprite;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Threading;
namespace SpaceFish
{
    public class PathClass
    {
        private  Texture2D dot;
        private  Texture2D red;

        private  string[,] thePath = new string[25, 15];

        private  List<string[,]> thePreviousRoads = new List<string[,]>();

        
        private  List<SpriteClass> sprites = new List<SpriteClass>();

        private  List<SpriteClass> sprites2 = new List<SpriteClass>();

        private  Vector2 currentTile;
        private  Vector2 previousTile;
        private  bool ispreviousTileASplit;

         Vector2 GetStartingPosition(string[,] theRoad)
        {
            //initialise the return value to something off of the level
            Vector2 startingPos = new Vector2(99, 99);
            for (int y = 0; y < 15; y++)
            {//row
                for (int x = 0; x < 25; x++)
                {//column
                    if (theRoad[x, y] == "start")//check if tile is the starting pne
                    {
                        startingPos.X = x;
                        startingPos.Y = y;
                        //if it cant find start it will return 99,99
                        return startingPos;
                    }
                }
            }
            return startingPos;//if it doesnt find it it will return 99,99 LOL
        }

        public  void InitialisePath(string[,] theRoad, ContentManager Contentm)
        {
            dot = Contentm.Load<Texture2D>("Content/Graphics/TileSheets/dot");
            red = Contentm.Load<Texture2D>("Content/Graphics/TileSheets/red");

            //make a copy of the 2d-array
            thePath = theRoad;

            //find the starting position
            previousTile = currentTile = GetStartingPosition(theRoad);
           

            //set the start tile == "currenttile"
            thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
            sprites.Add(new SpriteClass(dot, new Vector2(currentTile.X * 32, currentTile.Y * 32), true));
            //take a copy of the origonal road before any modifacations
            thePreviousRoads.Add(thePath);

        }

        public  void CanIDrawHere(float mouseX, float mouseY)
        {
            //convert the clicked position to a position in the array
            int arrayX = (int)mouseX / 32;
            int arrayY = (int)mouseY / 32;

            Debug.WriteLine("X: " + arrayX + " Y: " + arrayY);

            //if the position pressed is not empty
            if (thePath[arrayX, arrayY] != "")
            {
                //make the currentTile this tile
                currentTile.X = arrayX;
                currentTile.Y = arrayY;

                //if this is not the same tile we just pressed
                if (currentTile != previousTile)
                {
                    #region minus the north
                    //if at the top of the screen, dont check the north
                    if (arrayY == 0)
                    {
                        //check around for the current tile 
                        if ((thePath[arrayX + 1, arrayY] == "currenttile") || //to the east
                           (thePath[arrayX, arrayY - 1] == "currenttile") || //to the south
                           (thePath[arrayX - 1, arrayY] == "currenttile"))   //to the west
                        {
                            //check that this tile has not been previously visited
                            if (thePath[arrayX, arrayY] != "chosenpath" &&
                                thePath[arrayX, arrayY] != "visited")
                            {
                                //if the previous tile was a split tile
                                if (ispreviousTileASplit)
                                {
                                    //take acopy of the current path b4 split
                                    thePreviousRoads.Add(thePath);
                                    //and the current the current tile
                                    //make the previous tile a visited tile
                                    thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                    thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";

                                    //block the other paths
                                    MakePathBlockedV2(currentTile, previousTile, true);
                                }

                                //check if this tile is a split tile
                                if (thePath[arrayX, arrayY] == "split")
                                {
                                    //make the current tile a visited tile
                                    thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                    thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";
                                    //and set this to true for the next loop
                                    ispreviousTileASplit = true;
                                }
                                else
                                {
                                    //and set this to false for the next loop
                                    ispreviousTileASplit = false;
                                }

                                //make the current tile the "currenttile"
                                //and the previous tile a "chosenpath" tile
                                thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";

                                //set previousTile to currentTile for the next loop
                                previousTile.X = currentTile.X;
                                previousTile.Y = currentTile.Y;

                                //add a dot to mark where the player has been
                                sprites.Add(new SpriteClass(dot, new Vector2(currentTile.X * 32, currentTile.Y * 32), true));

                            }//previously visited check
                        }//found current tile check 
                    }//north bound check
                    #endregion
                    #region minus the east
                    //if at the right of the screen, dont check the east
                    else if (arrayX == 24)
                    {
                        //check around for the current tile 
                        if ((thePath[arrayX, arrayY-1] == "currenttile") || //to the north
                           (thePath[arrayX, arrayY - 1] == "currenttile") || //to the south
                           (thePath[arrayX - 1, arrayY] == "currenttile"))   //to the west
                        {
                            //check that this tile has not been previously visited
                            if (thePath[arrayX, arrayY] != "chosenpath" &&
                                thePath[arrayX, arrayY] != "visited")
                            {
                                //if the previous tile was a split tile
                                if (ispreviousTileASplit)
                                {
                                    //take acopy of the current path b4 split
                                    thePreviousRoads.Add(thePath);
                                    //and the current the current tile
                                    //make the previous tile a visited tile
                                    thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                    thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";

                                    //block the other paths
                                    MakePathBlockedV2(currentTile, previousTile, true);
                                }

                                //check if this tile is a split tile
                                if (thePath[arrayX, arrayY] == "split")
                                {
                                    //make the current tile a visited tile
                                    thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                    thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";
                                    //and set this to true for the next loop
                                    ispreviousTileASplit = true;
                                }
                                else
                                {
                                    //and set this to false for the next loop
                                    ispreviousTileASplit = false;
                                }

                                //make the current tile the "currenttile"
                                //and the previous tile a "chosenpath" tile
                                thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";

                                //set previousTile to currentTile for the next loop
                                previousTile.X = currentTile.X;
                                previousTile.Y = currentTile.Y;

                                //add a dot to mark where the player has been
                                sprites.Add(new SpriteClass(dot, new Vector2(currentTile.X * 32, currentTile.Y * 32), true));

                            }//previously visited check
                        }//found current tile check 
                    }//east bound check
                    #endregion
                    #region minus the south
                    //if at the top of the screen, dont check the north
                    else if (arrayY == 14)
                    {
                        //check around for the current tile 
                        if ((thePath[arrayX, arrayY-1] == "currenttile") || //to the north
                           (thePath[arrayX+1, arrayY] == "currenttile") || //to the east
                           (thePath[arrayX - 1, arrayY] == "currenttile"))   //to the west
                        {
                            //check that this tile has not been previously visited
                            if (thePath[arrayX, arrayY] != "chosenpath" &&
                                thePath[arrayX, arrayY] != "visited")
                            {
                                //if the previous tile was a split tile
                                if (ispreviousTileASplit)
                                {
                                    //take acopy of the current path b4 split
                                    thePreviousRoads.Add(thePath);
                                    //and the current the current tile
                                    //make the previous tile a visited tile
                                    thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                    thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";

                                    //block the other paths
                                    MakePathBlockedV2(currentTile, previousTile, true);
                                }

                                //check if this tile is a split tile
                                if (thePath[arrayX, arrayY] == "split")
                                {
                                    //make the current tile a visited tile
                                    thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                    thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";
                                    //and set this to true for the next loop
                                    ispreviousTileASplit = true;
                                }
                                else
                                {
                                    //and set this to false for the next loop
                                    ispreviousTileASplit = false;
                                }

                                //make the current tile the "currenttile"
                                //and the previous tile a "chosenpath" tile
                                thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";

                                //set previousTile to currentTile for the next loop
                                previousTile.X = currentTile.X;
                                previousTile.Y = currentTile.Y;

                                //add a dot to mark where the player has been
                                sprites.Add(new SpriteClass(dot, new Vector2(currentTile.X * 32, currentTile.Y * 32), true));

                            }//previously visited check
                        }//found current tile check 
                    }//south bound check
                    #endregion
                    #region minus the west
                    //if at the right of the screen, dont check the east
                    else if (arrayX == 0)
                    {
                        //check around for the current tile 
                        if ((thePath[arrayX, arrayY - 1] == "currenttile") || //to the north
                           (thePath[arrayX + 1, arrayY] == "currenttile") || //to the east
                           (thePath[arrayX, arrayY + 1] == "currenttile"))   //to the south
                        {
                            //check that this tile has not been previously visited
                            if (thePath[arrayX, arrayY] != "chosenpath" &&
                                thePath[arrayX, arrayY] != "visited")
                            {
                                //if the previous tile was a split tile
                                if (ispreviousTileASplit)
                                {
                                    //take acopy of the current path b4 split
                                    thePreviousRoads.Add(thePath);
                                    //and the current the current tile
                                    //make the previous tile a visited tile
                                    thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                    thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";

                                    //block the other paths
                                    MakePathBlockedV2(currentTile, previousTile, true);
                                }

                                //check if this tile is a split tile
                                if (thePath[arrayX, arrayY] == "split")
                                {
                                    //make the current tile a visited tile
                                    thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                    thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";
                                    //and set this to true for the next loop
                                    ispreviousTileASplit = true;
                                }
                                else
                                {
                                    //and set this to false for the next loop
                                    ispreviousTileASplit = false;
                                }

                                //make the current tile the "currenttile"
                                //and the previous tile a "chosenpath" tile
                                thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";

                                //set previousTile to currentTile for the next loop
                                previousTile.X = currentTile.X;
                                previousTile.Y = currentTile.Y;

                                //add a dot to mark where the player has been
                                sprites.Add(new SpriteClass(dot, new Vector2(currentTile.X * 32, currentTile.Y * 32), true));

                            }//previously visited check
                        }//found current tile check 
                    }//west bound check
                    #endregion
                    #region if not at the eadge of the screen
                    //if not at the eadge of the screen
                    else
                    {
                        //check around for the current tile 
                        if ((thePath[arrayX, arrayY - 1] == "currenttile") || //to the north
                           (thePath[arrayX + 1, arrayY] == "currenttile") || //to the east
                           (thePath[arrayX, arrayY + 1] == "currenttile") ||  //to the south
                           (thePath[arrayX - 1, arrayY] == "currenttile")) // ot the west
                        {
                            //check that this tile has not been previously visited
                            if (thePath[arrayX, arrayY] != "chosenpath" &&
                                thePath[arrayX, arrayY] != "visited")
                            {
                                //if the previous tile was a split tile
                                if (ispreviousTileASplit)
                                {
                                    //take acopy of the current path b4 split
                                    thePreviousRoads.Add(thePath);
                                    //and the current the current tile
                                    //make the previous tile a visited tile
                                    thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                    thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";

                                    //block the other paths
                                    MakePathBlockedV2(currentTile, previousTile, true);
                                }

                                //check if this tile is a split tile
                                if (thePath[arrayX, arrayY] == "split")
                                {
                                    //make the current tile a visited tile
                                    thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                    thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";
                                    //and set this to true for the next loop
                                    ispreviousTileASplit = true;
                                }
                                else
                                {
                                    //and set this to false for the next loop
                                    ispreviousTileASplit = false;
                                }

                                //make the current tile the "currenttile"
                                //and the previous tile a "chosenpath" tile
                                thePath[(int)currentTile.X, (int)currentTile.Y] = "currenttile";
                                thePath[(int)previousTile.X, (int)previousTile.Y] = "chosenpath";

                                //set previousTile to currentTile for the next loop
                                previousTile.X = currentTile.X;
                                previousTile.Y = currentTile.Y;

                                //add a dot to mark where the player has been
                                sprites.Add(new SpriteClass(dot, new Vector2(currentTile.X * 32, currentTile.Y * 32), true));

                            }//previously visited check
                        }//found current tile check 
                    }//if not at the eadge
                    #endregion
                }// previous tile check
            }//tile not empty check
        }//end CanIDrawHere() function

        public  void draw()
        {
            foreach (SpriteClass sprite in sprites)
            {
                sprite.Draw();

            }
            foreach (SpriteClass sprite2 in sprites2)
            {
                sprite2.Draw();
            }
        }

        
        private  Vector2 MakePathBlockedV2(Vector2 currentPos, Vector2 previousPos, bool isFistCallingOfThisFunction)
        {
            //local lists
            //list to store the starting positions of the possible paths found
            List<Vector2> foundPaths = new List<Vector2>();
            //list to store the path indicator of the possible paths found
            List<string> foundPathsIndicator = new List<string>();

            //if this is the first time this function hasbeen called
            //e.g. when the player has selected a path from a split
            #region fist call of this function
            if (isFistCallingOfThisFunction)
            {
                #region find the paths
                //find all other possible paths
                //only check the north if its not on the eadge
                if (previousPos.Y > 0)
                {
                    //check to the north of the split
                    if ((thePath[(int)previousPos.X, (int)previousPos.Y - 1] != "currenttile") &&   //if its not the current tile
                        (thePath[(int)previousPos.X, (int)previousPos.Y - 1] != "chosenpath") &&    //or the chosen path
                        (thePath[(int)previousPos.X, (int)previousPos.Y - 1] != "visited") &&       //or a previously visited tile
                        (thePath[(int)previousPos.X, (int)previousPos.Y - 1] != ""))                //or an empty field
                    {
                        //take a note of this position and the path indicator
                        foundPaths.Add(new Vector2(previousPos.X, previousPos.Y - 1));
                        foundPathsIndicator.Add(thePath[(int)previousPos.X, (int)previousPos.Y - 1]);
                    }
                }

                //only check the east if its not on the eadge
                if (previousPos.X < 24)
                {
                    //check to the east of the split
                    if ((thePath[(int)previousPos.X + 1, (int)previousPos.Y] != "currenttile") &&
                       (thePath[(int)previousPos.X + 1, (int)previousPos.Y] != "chosenpath") &&
                       (thePath[(int)previousPos.X + 1, (int)previousPos.Y] != "visited") &&
                       (thePath[(int)previousPos.X + 1, (int)previousPos.Y] != ""))
                    {
                        //take a note of this position and the path indicator
                        foundPaths.Add(new Vector2(previousPos.X + 1, previousPos.Y));
                        foundPathsIndicator.Add(thePath[(int)previousPos.X + 1, (int)previousPos.Y]);
                    }
                }

                //only check the south if its not on the eadge
                if (previousPos.Y < 14)
                {
                    //check to the south of the split
                    if ((thePath[(int)previousPos.X, (int)previousPos.Y + 1] != "currenttile") &&
                       (thePath[(int)previousPos.X, (int)previousPos.Y + 1] != "chosenpath") &&
                       (thePath[(int)previousPos.X, (int)previousPos.Y + 1] != "visited") &&
                       (thePath[(int)previousPos.X, (int)previousPos.Y + 1] != ""))
                    {
                        //take a note of this position and the path indicator
                        foundPaths.Add(new Vector2(previousPos.X, previousPos.Y + 1));
                        foundPathsIndicator.Add(thePath[(int)previousPos.X, (int)previousPos.Y + 1]);
                    }
                }

                //only check the west if its not on the eadge
                if (previousPos.X > 0)
                {
                    //check to the west of the split
                    if ((thePath[(int)previousPos.X - 1, (int)previousPos.Y] != "currenttile") &&
                       (thePath[(int)previousPos.X - 1, (int)previousPos.Y] != "chosenpath") &&
                       (thePath[(int)previousPos.X - 1, (int)previousPos.Y] != "visited") &&
                       (thePath[(int)previousPos.X - 1, (int)previousPos.Y] != ""))
                    {
                        //take a note of this position and the path indicator
                        foundPaths.Add(new Vector2(previousPos.X - 1, previousPos.Y));
                        foundPathsIndicator.Add(thePath[(int)previousPos.X - 1, (int)previousPos.Y]);
                    }
                }
                #endregion
                #region go though each path found
                //go though each path found
                int path = 0;
                foreach (Vector2 pathStartPosition in foundPaths)
                {
                    //set the current to the starting position
                    currentPos.X = pathStartPosition.X;
                    currentPos.Y = pathStartPosition.Y;

                    //while the current tile isn't at the join tile
                    while (thePath[(int)currentPos.X, (int)currentPos.Y] != "join")
                    {
                        //if the foundPathsIndicator == "path1" then
                        #region path1
                        if (foundPathsIndicator[path] == "path1")
                        {
                            //check the surrrounding tiles for the next path1 or split piece
                            #region north
                            //only check the north if its not on the eadge
                            if (currentPos.Y > 0)
                            {
                                //if the tile to the north is path1
                                if ((thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "path1")) //to the north 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.Y -= 1;

                                }
                                //if the tile to the north is a split instead
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.Y -= 1;

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));
                                }
                                //if the tile to the north is a join
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.Y -= 1;

                                    //the exit by return ing the current position which is now at the next 
                                    return currentPos;
                                }
                            }
                            #endregion//end north

                            #region east
                            //only check the east if its not on the eadge
                            if (currentPos.X < 24)
                            {
                                //if the tile to the east is path1
                                if ((thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "path1")) //to the east 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.X += 1;

                                }
                                //if the tile to the east is a split instead
                                else if (thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.X += 1;

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                }
                                //if the tile to the east is a join
                                else if (thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.X += 1;

                                    //the exit by return ing the current position which is now at the next 
                                    return currentPos;
                                }
                            }
                            #endregion//end east

                            #region south
                            //only check the south if its not on the eadge
                            if (currentPos.Y < 14)
                            {
                                //if the tile to the south is path1
                                if ((thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "path1")) //to the south 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.Y += 1;

                                }
                                //if the tile to the south is a split instead
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.Y += 1;

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));
                                }
                                //if the tile to the south is a join
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.Y += 1;

                                    //the exit by return ing the current position which is now at the next 
                                    return currentPos;
                                }
                            }
                            #endregion//end south

                            #region west
                            //only check the west if its not on the eadge
                            if (currentPos.X > 0)
                            {
                                //if the tile to the west is path1
                                if ((thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "path1")) //to the west 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.X -= 1;

                                }
                                //if the tile to the west is a split instead
                                else if (thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.X -= 1;

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));
                                }
                                //if the tile to the north is a join
                                else if (thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.X -= 1;

                                    //the exit by return ing the current position which is now at the next 
                                    return currentPos;
                                }
                            }
                            #endregion//end west
                        }
                        #endregion//end path1
                        #region path2
                        if (foundPathsIndicator[path] == "path2")
                        {
                            //check the surrrounding tiles for the next path2 or split piece
                            #region north
                            //only check the north if its not on the eadge
                            if (currentPos.Y > 0)
                            {
                                //if the tile to the north is path2
                                if ((thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "path2")) //to the north 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.Y -= 1;

                                }
                                //if the tile to the north is a split instead
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.Y -= 1;

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));
                                }
                                //if the tile to the north is a join
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.Y -= 1;

                                    //the exit by return ing the current position which is now at the next 
                                    return currentPos;
                                }
                            }//end boundries check
                            #endregion//end north

                            #region east
                            //only check the east if its not on the eadge
                            if (currentPos.X < 24)
                            {
                                //if the tile to the east is path2
                                if ((thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "path2")) //to the east 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.X += 1;

                                }
                                //if the tile to the east is a split instead
                                else if (thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.X += 1;

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));
                                    
                                }
                                //if the tile to the east is a join
                                else if (thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.X += 1;

                                    //the exit by return ing the current position which is now at the next 
                                    return currentPos;
                                }
                            }
                            #endregion//end east

                            #region south
                            //only check the north if its not on the eadge
                            if (currentPos.Y < 14)
                            {
                                //if the tile to the south is path2
                                if ((thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "path2")) //to the south 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.Y += 1;

                                }
                                //if the tile to the south is a split instead
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.Y += 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));
                                }
                                //if the tile to the south is a join
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.Y += 1;

                                    //the exit by return ing the current position which is now at the next 
                                    return currentPos;
                                }
                            }
                            #endregion//end south

                            #region west
                            //only check the west if its not on the eadge
                            if (currentPos.X > 0)
                            {
                                //if the tile to the west is path2
                                if ((thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "path2")) //to the west 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.X -= 1;

                                }
                                //if the tile to the west is a split instead
                                else if (thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.X -= 1;

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));
                                }
                                //if the tile to the north is a join
                                else if (thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.X -= 1;

                                    //the exit by return ing the current position which is now at the next 
                                    return currentPos;
                                }
                            }
                            #endregion//end west
                        }
                        #endregion//end path2
                        #region path3
                        if (foundPathsIndicator[path] == "path3")
                        {
                            //check the surrrounding tiles for the next path3 or split piece
                            #region north
                            //only check the North if the current tile is not on the eadge
                            if (currentPos.Y > 0)
                            {
                                //if the tile to the north is path3
                                if ((thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "path3")) //to the north 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.Y -= 1;

                                }
                                //if the tile to the north is a split instead
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.Y -= 1;

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));
                                }
                                //if the tile to the north is a join
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.Y -= 1;

                                    //the exit by return ing the current position which is now at the next 
                                    return currentPos;
                                }
                            }
                            #endregion//end north

                            #region east
                            //only check the east if the current tile is not on the eadge
                            if (currentPos.X < 24)
                            {
                                //if the tile to the east is path3
                                if ((thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "path3")) //to the east 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.X += 1;

                                }
                                //if the tile to the east is a split instead
                                else if (thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.X += 1;

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));
                                }
                                //if the tile to the east is a join
                                else if (thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.X += 1;

                                    //the exit by return ing the current position which is now at the next 
                                    return currentPos;
                                }
                            }
                            #endregion//end east

                            #region south
                            //only check the south if the current tile is not on the eadge
                            if (currentPos.Y < 14)
                            {
                                //if the tile to the south is path3
                                if ((thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "path3")) //to the south 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.Y += 1;

                                }
                                //if the tile to the south is a split instead
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.Y += 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));
                                }
                                //if the tile to the south is a join
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.Y += 1;

                                    //the exit by return ing the current position which is now at the next 
                                    return currentPos;
                                }
                            }
                            #endregion//end south

                            #region west
                            //only check the west if the current tile is not on the eadge
                            if (currentPos.X > 0)
                            {
                                //if the tile to the west is path3
                                if ((thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "path3")) //to the west 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.X -= 1;

                                }
                                //if the tile to the west is a split instead
                                else if (thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.X -= 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //place a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));
                                }
                                //if the tile to the west is a join
                                else if (thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.X -= 1;

                                    //the exit by return ing the current position which is now at the next 
                                    return currentPos;
                                }
                            }
                            #endregion//end west
                        }
                        #endregion//end path3
                    }//end while
                    path++;
                }//end foreach
                #endregion

            }//end bool check
            #endregion//end fist call of this function
            //if this has been called by itself
            #region called from within this function
            else
            {
                #region find the paths
                //find all paths around the split tile
                //only check the North if the split tile is not on the eadge
                if (currentPos.Y > 0)
                {
                    //check to the north of the split
                    if ((thePath[(int)currentPos.X, (int)currentPos.Y - 1] != "currenttile") &&   //if its not the current tile
                        (thePath[(int)currentPos.X, (int)currentPos.Y - 1] != "chosenpath") &&    //or the chosen path
                        (thePath[(int)currentPos.X, (int)currentPos.Y - 1] != "visited") &&       //or a previously visited tile
                        (thePath[(int)currentPos.X, (int)currentPos.Y - 1] != ""))                //or an empty field
                    {
                        //take a note of this position and the path indicator
                        foundPaths.Add(new Vector2(currentPos.X, currentPos.Y - 1));
                        foundPathsIndicator.Add(thePath[(int)currentPos.X, (int)currentPos.Y - 1]);
                    }
                }

                //only check the east if the split tile is not on the eadge
                if (currentPos.X < 24)
                {
                    //check to the east of the split
                    if ((thePath[(int)currentPos.X + 1, (int)currentPos.Y] != "currenttile") &&
                       (thePath[(int)currentPos.X + 1, (int)currentPos.Y] != "chosenpath") &&
                       (thePath[(int)currentPos.X + 1, (int)currentPos.Y] != "visited") &&
                       (thePath[(int)currentPos.X + 1, (int)currentPos.Y] != ""))
                    {
                        //take a note of this position and the path indicator
                        foundPaths.Add(new Vector2(currentPos.X + 1, currentPos.Y));
                        foundPathsIndicator.Add(thePath[(int)currentPos.X + 1, (int)currentPos.Y]);
                    }
                }

                //only check the south if the split tile is not on the eadge
                if (currentPos.Y < 14)
                {
                    //check to the south of the split
                    if ((thePath[(int)currentPos.X, (int)currentPos.Y + 1] != "currenttile") &&
                       (thePath[(int)currentPos.X, (int)currentPos.Y + 1] != "chosenpath") &&
                       (thePath[(int)currentPos.X, (int)currentPos.Y + 1] != "visited") &&
                       (thePath[(int)currentPos.X, (int)currentPos.Y + 1] != ""))
                    {
                        //take a note of this position and the path indicator
                        foundPaths.Add(new Vector2(currentPos.X, currentPos.Y + 1));
                        foundPathsIndicator.Add(thePath[(int)currentPos.X, (int)currentPos.Y + 1]);
                    }
                }

                //only check the west if the split tile is not on the eadge
                if (currentPos.X > 0)
                {
                    //check to the west of the split
                    if ((thePath[(int)currentPos.X - 1, (int)currentPos.Y] != "currenttile") &&
                       (thePath[(int)currentPos.X - 1, (int)currentPos.Y] != "chosenpath") &&
                       (thePath[(int)currentPos.X - 1, (int)currentPos.Y] != "visited") &&
                       (thePath[(int)currentPos.X - 1, (int)currentPos.Y] != ""))
                    {
                        //take a note of this position and the path indicator
                        foundPaths.Add(new Vector2(currentPos.X - 1, currentPos.Y));
                        foundPathsIndicator.Add(thePath[(int)currentPos.X - 1, (int)currentPos.Y]);
                    }
                }
                #endregion//end find all the paths

                #region go though each path
                int pathV2 = 0;
                foreach (Vector2 pathStartPosition in foundPaths)
                {
                    //set the current to the starting position
                    currentPos.X = pathStartPosition.X;
                    currentPos.Y = pathStartPosition.Y;

                    //while the current tile isn't at the join tile
                    while (thePath[(int)currentPos.X, (int)currentPos.Y] != "join")
                    {
                        //if the foundPathsIndicator == "path1" then
                        #region path1
                        if (foundPathsIndicator[pathV2] == "path1")
                        {
                            //check the surrrounding tiles for the next piece
                            #region north
                            //only check the North if the split tile is not on the eadge
                            if (currentPos.Y > 0)
                            {
                                //if the tile to the north is path1
                                if ((thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "path1")) //to the north 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.Y -= 1;

                                }
                                //if the tile to the north is a split instead
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "split")//if its a split
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //play a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.Y -= 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                }
                                //if the tile to the north is a join
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.Y -= 1;

                                    //break out the while loop for this path
                                    break;
                                }
                            }
                            #endregion//end north

                            #region east
                            //only check the east if the split tile is not on the eadge
                            if (currentPos.X < 24)
                            {
                                //if the tile to the east is path1
                                if ((thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "path1")) //to the east 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.X += 1;

                                }
                                //if the tile to the east is a split instead
                                else if (thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //play a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.X += 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                }
                                //if the tile to the east is a join
                                else if (thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.X += 1;

                                    //break out the while loop for this path
                                    break;
                                }
                            }
                            #endregion//end east

                            #region south
                            //only check the south if the split tile is not on the eadge
                            if (currentPos.Y < 14)
                            {
                                //if the tile to the south is path1
                                if ((thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "path1")) //to the south 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.Y += 1;

                                }
                                //if the tile to the south is a split instead
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //play a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.Y += 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                }
                                //if the tile to the south is a join
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.Y += 1;

                                    //break out the while loop for this path 
                                    break;
                                }
                            }
                            #endregion//end south

                            #region west
                            //only check the west if the split tile is not on the eadge
                            if (currentPos.X > 0)
                            {
                                //if the tile to the west is path1
                                if ((thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "path1")) //to the west 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.X -= 1;

                                }
                                //if the tile to the west is a split instead
                                else if (thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //play a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.X -= 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";
                                }
                                //if the tile to the north is a join
                                else if (thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.X -= 1;

                                    //break out the while loop for this path
                                    break;
                                }
                            }
                            #endregion//end west
                        }
                        #endregion//end path1

                        //if the foundPathsIndicator == "path2" then
                        #region path2
                        if (foundPathsIndicator[pathV2] == "path2")
                        {
                            //check the surrrounding tiles for the next piece
                            #region north
                            //if the Y position is 0 dont check above it
                            if (currentPos.Y > 0)
                            {
                                //if the tile to the north is path2
                                if ((thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "path2")) //to the north 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.Y -= 1;

                                }
                                //if the tile to the north is a split instead
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //play a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.Y -= 1;

                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";
                                }
                                //if the tile to the north is a join
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.Y -= 1;

                                    //break out the while loop for this path
                                    break;
                                }
                            }
                            #endregion//end north

                            #region east
                            //only check the North if the split tile is not on the eadge
                            if (currentPos.X < 24)
                            {
                                //if the tile to the east is path2
                                if ((thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "path2")) //to the east 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.X += 1;

                                }
                                //if the tile to the east is a split instead
                                else if (thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //play a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.X += 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                }
                                //if the tile to the east is a join
                                else if (thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.X += 1;

                                    //break out the while loop for this path
                                    break;
                                }
                            }
                            #endregion//end east

                            #region south
                            //only check the North if the split tile is not on the eadge
                            if (currentPos.Y < 14)
                            {
                                //if the tile to the south is path2
                                if ((thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "path2")) //to the south 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.Y += 1;

                                }
                                //if the tile to the south is a split instead
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //play a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.Y += 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";
                                }
                                //if the tile to the south is a join
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.Y += 1;

                                    //break out the while loop for this path 
                                    break;
                                }
                            }
                            #endregion//end south

                            #region west
                            //only check the North if the split tile is not on the eadge
                            if (currentPos.X > 0)
                            {
                                //if the tile to the west is path2
                                if ((thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "path2")) //to the west 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.X -= 1;

                                }
                                //if the tile to the west is a split instead
                                else if (thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //play a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.X -= 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";
                                }
                                //if the tile to the north is a join
                                else if (thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.X -= 1;

                                    //break out the while loop for this path
                                    break;
                                }
                            }
                            #endregion//end west
                        }
                        #endregion//end path2

                        //if the foundPathsIndicator == "path3" then
                        #region path3
                        if (foundPathsIndicator[pathV2] == "path3")
                        {
                            //check the surrrounding tiles for the next piece
                            #region north
                            //only check the North if the split tile is not on the eadge
                            if (currentPos.Y > 0)
                            {
                                //if the tile to the north is path3
                                if ((thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "path3")) //to the north 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.Y -= 1;

                                }
                                //if the tile to the north is a split instead
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //play a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.Y -= 1;

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";
                                }
                                //if the tile to the north is a join
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y - 1] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.Y -= 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //break out the while loop for this path
                                    break;
                                }
                            }
                            #endregion//end north

                            #region east
                            //only check the North if the split tile is not on the eadge
                            if (currentPos.X < 24)
                            {
                                //if the tile to the east is path3
                                if ((thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "path3")) //to the east 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.X += 1;

                                }
                                //if the tile to the east is a split instead
                                else if (thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //play a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.X += 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";
                                }
                                //if the tile to the east is a join
                                else if (thePath[(int)currentPos.X + 1, (int)currentPos.Y] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.X += 1;

                                    //break out the while loop for this path
                                    break;
                                }
                            }
                            #endregion//end east

                            #region south
                            //only check the North if the split tile is not on the eadge
                            if (currentPos.Y < 14)
                            {
                                //if the tile to the south is path3
                                if ((thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "path3")) //to the south 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.Y += 1;

                                }
                                //if the tile to the south is a split instead
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //play a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.Y += 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";
                                }
                                //if the tile to the south is a join
                                else if (thePath[(int)currentPos.X, (int)currentPos.Y + 1] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.Y += 1;

                                    //break out the while loop for this path 
                                    break;
                                }
                            }
                            #endregion//end south

                            #region west
                            //only check the North if the split tile is not on the eadge
                            if (currentPos.X > 0)
                            {
                                //if the tile to the west is path3
                                if ((thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "path3")) //to the west 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to this new pos
                                    currentPos.X -= 1;

                                }
                                //if the tile to the west is a split instead
                                else if (thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "split") //if its a split 
                                {
                                    //make the current position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //play a red marker here
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //update the previous position to be the current position
                                    previousPos.X = currentPos.X;
                                    previousPos.Y = currentPos.Y;

                                    //update the current position to the tile with the split
                                    currentPos.X -= 1;

                                    //make the split position visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //call the split function
                                    currentPos = MakePathBlockedV2(currentPos, previousPos, false);

                                    //set the new current position (where the join is) to be visited
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";
                                }
                                //if the tile to the north is a join
                                else if (thePath[(int)currentPos.X - 1, (int)currentPos.Y] == "join")
                                {
                                    //make the previous position the current position
                                    previousPos = currentPos;

                                    //make the current position "visited"
                                    thePath[(int)currentPos.X, (int)currentPos.Y] = "visited";

                                    //draw a red marker at current position
                                    sprites2.Add(new SpriteClass(red, new Vector2(currentPos.X * 32, currentPos.Y * 32), true));

                                    //change current position to the join tile
                                    currentPos.X -= 1;

                                    //break out the while loop for this path
                                    break;
                                }
                            }
                            #endregion//end west
                        }
                        #endregion//end path3
                    }//end while
                    pathV2++;
                }//end foreach
                #endregion
            }//end else
            #endregion//end called from within this function

            //once we have done everything, return the current position
            //which is the end of the split
            return currentPos;
        }//end function

        public  void ClearSprites()
        {
            sprites.Clear();
            sprites2.Clear();
        }

        public  string[,] ReturnList()
        {
            return thePath;
        }
    }//end class
}//end namespace



