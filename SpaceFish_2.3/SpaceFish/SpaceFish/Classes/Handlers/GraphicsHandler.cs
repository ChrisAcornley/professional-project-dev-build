#region NAMESPACES

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using SpaceFish.Graphics.Font;
using SpaceFish.Graphics.LoadingScreen;

#endregion

// Creates class inside the SpaceFish namespace
namespace SpaceFish
{
    /// <summary>
    /// Declares the graphics handler class
    /// </summary>
    public class GraphicsHandler
    {
       #region INSTANCES

        // Creates the graphics device manager so a graphics device can be managed
       private GraphicsDeviceManager graphicsDeviceManager;

       // Creates an instance of the GameFont class
       public GameFont gameFont;

       // Creates an instance of the LoadingScreen class
       public LoadingScreen loadingScreen;

       // Creates a graphics device for all graphical needs
       private GraphicsDevice graphicsDevice;

       #endregion

       #region FUNCTIONS

       /// <summary>
       /// Constructor
       /// </summary>
       /// 
       /// <param name="game">
       /// (Main) Passes in the game for the graphics device manager can be created
       /// </param>
       public GraphicsHandler(Main game)
       {
           // Initialises the graphics device manager
           graphicsDeviceManager = new GraphicsDeviceManager(game);

           // Sets the program to full screen
           graphicsDeviceManager.IsFullScreen = true;
       }

       /// <summary>
       /// Intialises the main parts of the graphics handler
       /// </summary>
       /// 
       /// <param name="tempDevice">
       /// (GraphicsDevice) Passes in the graphics device for management
       /// </param>
       /// 
       /// <returns>
       /// (Bool) Returns a boolean depending on whether the initialisation of 
       /// the class is successful
       /// </returns>
       public bool Initialise(GraphicsDevice tempDevice)
       {
           // Sets the graphics device to the passes device
           graphicsDevice = tempDevice;
           // Checks if device is valid and returns false if not
           if (graphicsDevice == null)
               return false;

           // Initialises the instance of GameFont
           gameFont = new GameFont();
           // Checks if class if valid and returns false if not
           if(gameFont == null)
               return false;

           // Initialises the instance of LoadingScreen
           loadingScreen = new LoadingScreen();
           // Checks if class is valid and returns false if not
           if(loadingScreen == null)
               return false;

           // Sets the back buffer size of the device to 800x480 pixels
           graphicsDeviceManager.PreferredBackBufferWidth = 800;
           graphicsDeviceManager.PreferredBackBufferHeight = 480;

           // Sets the orientation of the device to be landscape and starting on the left hand size of the device
           graphicsDeviceManager.SupportedOrientations = DisplayOrientation.LandscapeLeft;

           // If all intialisation is true then initialisation has passed
           return true;
       }

       /// <summary>
       /// Clears the graphics device with a set colour
       /// </summary>
       /// 
       /// <param name="passColor">
       /// (Color) Colour to clear the screen with
       /// </param>
       public void ClearGraphicsDevice(Color passColor)
       {
           // Clears the screen with a set colour
           graphicsDevice.Clear(passColor);
       }

       /// <summary>
       /// Returns the graphics device
       /// </summary>
       /// 
       /// <returns>
       /// (GraphicsDevice) Returns the device for the Main to use
       /// </returns>
       public GraphicsDevice GetGraphicsDevice()
       {
           return graphicsDevice;
       }

       /// <summary>
       /// Gets the device viewport for the program to draw in
       /// </summary>
       /// 
       /// <param name="tempViewport">
       /// (Texture2D) Passes in texture used for mapping the viewport
       /// </param>
       /// 
       /// <returns>
       /// (Vector2) Returns a vector for the viewport
       /// </returns>
       public Vector2 GetGraphicsDeviceViewport(Texture2D tempViewport)
       {
           // Creates temporary storage vector 
           Vector2 returner;

           // Intialises and calculates the values of the vector
           returner = new Vector2((graphicsDevice.Viewport.Width / 2) - (tempViewport.Width / 2), (graphicsDevice.Viewport.Height / 2) - (tempViewport.Height / 2));

           // Returns the vector data
           return returner;
       }

       #endregion
    }
}
