#region NAMESPACES

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

#endregion

// Namespace for the font data for the game
namespace SpaceFish.Graphics.Font
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class GameFont
    {
        #region INSTANCES

        // Data for the font
        private SpriteFont gameFont;

        #endregion

        #region FUNCTIONS

        /// <summary>
        /// Constructor
        /// </summary>
        public GameFont()
        {
            // Nothing needs to be started
        }

        /// <summary>
        /// Returns the sprite font data
        /// </summary>
        /// 
        /// <returns>
        /// (SpriteFont) Data for the text font
        /// </returns>
        public SpriteFont GetGameFont()
        {
            return gameFont;
        }

        /// <summary>
        /// Loads in content for the text data
        /// </summary>
        /// 
        /// <param name="fontContent">
        /// (ContentManager) Passes through the content manager so content can
        /// be loaded in.
        /// </param>
        public void LoadFontContent(ContentManager fontContent)
        {
            // Loads content into the text font
            gameFont = fontContent.Load<SpriteFont>("Content/Graphics/Fonts/Font");
        }

        #endregion
    }
}
