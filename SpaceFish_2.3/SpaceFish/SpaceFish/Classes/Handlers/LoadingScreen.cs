#region NAMESPACES

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

#endregion

// Namespace for the loading screen texture
namespace SpaceFish.Graphics.LoadingScreen
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class LoadingScreen
    {
        #region INSTANCES

        // Texture data for the loading screen
        private Texture2D loadingScreen;

        #endregion

        #region FUNCTIONS

        /// <summary>
        /// Constructor
        /// </summary>
        public LoadingScreen()
        {
            // Nothing to start with
        }

        /// <summary>
        /// Gets the loading screen data
        /// </summary>
        /// 
        /// <returns>
        /// (Texture2D) The texture data for the loading screen
        /// </returns>
        public Texture2D GetLoadingScreen()
        {
            return loadingScreen;
        }

        /// <summary>
        /// Loads the content for the loading screen
        /// </summary>
        /// 
        /// <param name="fontContent">
        /// (ContentManager) Passes the content manager to the class so it can
        /// load content
        /// </param>
        public void LoadLoadingScreenContent(ContentManager fontContent)
        {
            // Loads in texture data from the content folders
            loadingScreen = fontContent.Load<Texture2D>("Content/Graphics/LoadingThread/Loading");
        }

        #endregion
    }
}
