//////////////
// INCLUDES //
//////////////
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using SpaceFish;

// Create object in namespace
namespace SpaceFish
{
    // Create the class
    public class AudioHandler
    {
        #region Fields

        const int NUMBER_OF_SONGS = 3;  // Number of songs in the game

        List<Song> SongList;            // List of the songs

        int currentSong = 0;

        ContentManager audioContent;

        Main.GameState currentState;

        #endregion

        // Initialisation function
        public void Initialize()
        {
            // Create the song list to be able to store the song data
            SongList = new List<Song>(NUMBER_OF_SONGS);
        }

        // Play a specific song in the list
        public void PlaySong(int songNumber)
        {
            MediaPlayer.Play(SongList[songNumber]);
        }

        // Stop the media player
        public void StopPlaying()
        {
            MediaPlayer.Stop();
        }

        // Pause the media player
        public void PausePlaying()
        {
            MediaPlayer.Pause();
        }

        // Resumes the media player from where it was paused
        public void ResumePlaying()
        {
            MediaPlayer.Resume();
        }

        /// <summary>
        /// Load in a song to a specific point in the list.
        /// </summary>
        /// 
        /// <param name="songNumber">
        /// (int) Where in the list to load in the song.
        /// </param>
        /// 
        /// <param name="songData">
        /// (Song) The song data to loud in.
        /// </param>
        public void LoadInSongs(int songNumber, Song songData)
        {
            SongList.Insert(songNumber, songData);
        }

        /// <summary>
        /// Returns the current song the audio handler is running
        /// </summary>
        /// 
        /// <returns>
        /// (int) The current song in the list being played
        /// </returns>
        public int GetCurrentSong()
        {
            return currentSong;
        }

        /// <summary>
        /// Sets the current song for the audio handler should play.
        /// </summary>
        /// <param name="ncurrentSong">
        /// (int) Pass in the new song number so play.
        /// </param>
        public void SetCurrentSong(int ncurrentSong)
        {
            currentSong = ncurrentSong;
        }

        /// <summary>
        /// Moves to the next song in the list
        /// </summary>
        public void MoveToNextSong()
        {
            currentSong++;
        }

        /// <summary>
        /// Moves to the previous song in the list
        /// </summary>
        public void MoveToLastSong()
        {
            currentSong--;
        }
        
        /// <summary>
        /// Get the size of the song list.
        /// </summary>
        /// 
        /// <returns>
        /// (int) size of list
        /// </returns>
        public int GetSizeOfList()
        {
            return SongList.Count();
        }

        /// <summary>
        /// Loads in the audio content for the content manager.
        /// </summary>
        /// 
        /// <param name="tempContent">
        /// (ContentManager) The content manager is passed through so the audio manager can load
        /// in audio content
        /// </param>
        /// 
        /// <remarks>
        /// This can be increased as more content is needed. The function then
        /// cycles through all the songs in the SongList and checks that they 
        /// are not null. If it returns true then all data is correct. Else it
        /// returns false stating that some of the content has not loaded
        /// correctly.
        /// </remarks>
        /// 
        /// <returns>
        /// (boolean) Flag to check if data correctly loaded
        /// </returns>
        public bool LoadContent(ContentManager tempContent)
        {
            // Set content manager for the audio handler
            audioContent = tempContent;

            //load in audio songs
            LoadInSongs(0, audioContent.Load<Song>("Content/Audio/MainMenu/Menu"));
            LoadInSongs(1, audioContent.Load<Song>("Content/Audio/MainMenu/SpaceMenu"));
            LoadInSongs(2, audioContent.Load<Song>("Content/Audio/MainMenu/Above"));

            // Check all the audio has been successfully loaded
            for (int i = 0; i < NUMBER_OF_SONGS; i++)
            {
                if (SongList[i] == null)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Updates the audio manager.
        /// </summary>
        /// 
        /// <param name="tempState">
        /// (GameState) The gamestate of the program is passed to the audio manager
        /// </param>
        /// 
        /// <returns>
        /// (boolean) Flag to check update was successful
        /// </returns>
        public bool Update(Main.GameState tempState)
        {
            currentState = tempState;

            return true;
        }
    }
}
