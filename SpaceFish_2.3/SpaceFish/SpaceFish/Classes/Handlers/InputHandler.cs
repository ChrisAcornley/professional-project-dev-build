// Includes
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

// Create object in the main namespace
namespace SpaceFish
{
    // Create the class
    public class InputHandler
    {
        #region Variables

            // Create a private rectangle for the class
            private Rectangle mouseClickRect;
            
            // Create a mouse state for the current state and previous state
            public MouseState mouseState;
            private MouseState previousMouseState;

            // Create a result variable
            private bool result;

            // Create a flag to check if the user is all ready pressing
            private bool userPressing;

            // Create a flag to check if the user has clicked
            private bool userClicked;

        #endregion

        // Create initialisation function
        public bool Initialise(int nWidth, int nHeight)
        {
            // Set flags
            userPressing = false;

            // Create the rectangle
            mouseClickRect = new Rectangle();

            // Create mouse states and set values
            mouseState = new MouseState();
            previousMouseState = new MouseState();

            // If both mouse states are properly created set starting state
            if (mouseState != null && previousMouseState != null)
            {
                mouseState = Mouse.GetState();
                previousMouseState = mouseState;
                result = true;
            }

            // If both mouse states are not properly created
            else
            {
                result = false;
                return result;
            }

            // Checks to make the object was correctly created
            if (mouseClickRect != null)
            {
                // Set values
                mouseClickRect.Width = nWidth;
                mouseClickRect.Height = nHeight;
                mouseClickRect.X = mouseClickRect.Y = 0;

                // Sets the result
                result = true;
            }

            // If the object was not created
            else
            {
                result = false;
                return result;
            }

            return result;
        }
   
        // Set the touch location of the rectangle
        public void SetTouchLocation(int nX, int nY)
        {
            mouseClickRect.X = nX;
            mouseClickRect.Y = nY;
        }

        // Checks if the rectangle intersects with another rectangle
        public bool Intersect(Rectangle intersectRect)
        {
            // If there is an intersection return true
            if (mouseClickRect.Intersects(intersectRect))
            {
                return true;
            }

            // If there is no intersection return false
            else
            {
                return false;
            }
        }

        // Run through the updating of the input
        public void Update()
        {
            // Get mouse state
            mouseState = Mouse.GetState();

            // Set mouse position
            mouseClickRect.X = mouseState.X;
            mouseClickRect.Y = mouseState.Y;

            // Check if pressed
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                userPressing = true;
                userClicked = false;
            }
            
            // Check if clicked
            else if (previousMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
            {
                userPressing = false;
                userClicked = true;
            }

            // Set previous mouse state
            previousMouseState = mouseState;
        }

        // Get the clicked flag
        public bool GetIfClicked()
        {
            return userClicked;
        }

        // Check the pressed flag
        public bool GetIfPressed()
        {
            return userPressing;
        }

        // Pass the mouse state object
        public MouseState GetCurrentMouseState()
        {
            return mouseState;
        }
    }
}
